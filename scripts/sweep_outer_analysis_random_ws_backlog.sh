#!/bin/zsh

echo "file,x,y,seed,rate,fullness,stable,burst,wctotal" > sweep_random_srcq_analysis_ws_backlog.csv
	
parallel --bar --gnu -j32 --header : \
	'
	files=(random_2x2 random_3x3 random_4x4 random_5x5 random_6x6 random_7x7 random_8x8 random_12x12 random_16x16)
	pex=(2 3 4 5 6 7 8 12 16 )
	pey=(2 3 4 5 6 7 8 12 16 )

	file=$files[{i}]
	x=$pex[{i}]
	y=$pey[{i}]
	 ./sweep_inner_analysis_random_ws_backlog.sh $x $y $file {rate} {seed} {burst}
        fullness=`cat bin/rtflow_random_analysis_backlog/test_$x\_$y\_$file\_{rate}_{seed}_{burst}/buffer_max.csv`
        stable=`cat bin/rtflow_random_analysis_backlog/test_$x\_$y\_$file\_{rate}_{seed}_{burst}/stable.csv`
        wctotal=`cat bin/rtflow_random_analysis_backlog/test_$x\_$y\_$file\_{rate}_{seed}_{burst}/total_max.csv`
        
	sem --id mystr echo $file,$x,$y,{seed},{rate},$fullness,$stable,{burst},$wctotal >> sweep_random_srcq_analysis_ws_backlog.csv | cat'\
	::: i 4 \
	::: rate 5 6 8 A D 14 21 32 64 \
	::: seed `seq 0 99` \
        ::: burst 1 \

        #::: burst 1 2 4 8 \
