#!/bin/zsh


PE_X=$1
PE_Y=$2
BENCH=$3
RATE=$4
SEED=$5
BURST=$6
TS_BL=$7

mkdir -p bin/rtflow_random_analysis_wsbkp/test_${1}_${2}_${3}_${4}_${5}_${6}_${7}
pushd bin/rtflow_random_analysis_wsbkp/test_${1}_${2}_${3}_${4}_${5}_${6}_${7}

cp ../../../../bench/$BENCH-$SEED.dat .
cp ../../../../analyze_bp.py .
cp ../../../../top_extract_all_backpressure.m .
cp ../../../../west_fifo_network.m .
cp ../../../../west_fifo_extra_backpressure.m .
cp ../../../../compute_injection_latency.m .
cp ../../../../compute_injection_latency_bp.m .

RATE_D=`echo "ibase=16;obase=A;$RATE" | bc`
RATE_F=`echo "scale=3;1/$((RATE_D))" | bc`

python analyze_bp.py -N $PE_X -f $BENCH-$SEED.dat -B $BURST -R $RATE_F -TB $TS_BL

pwd
popd

