#!/bin/zsh


PE_X=$1
PE_Y=$2
BENCH=$3
RATE=$4
SEED=$5
BURST=$6

mkdir -p bin/rtflow_random_analysis_rt/test_${1}_${2}_${3}_${4}_${5}_${6}
pushd bin/rtflow_random_analysis_rt/test_${1}_${2}_${3}_${4}_${5}_${6}

cp ../../../../bench/$BENCH-$SEED.dat .
cp ../../../../python/* .

RATE_D=`echo "ibase=16;obase=A;$RATE" | bc`
RATE_F=`echo "scale=3;1/$((RATE_D))" | bc`

python hoprt.py -f $BENCH-$SEED.dat -B $BURST -R $RATE_F
python runnoc.py -s test.dat -o test_run.dat
python extract_rt.py -f test_run.dat

pwd
popd

