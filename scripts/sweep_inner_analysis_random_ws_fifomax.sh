#!/bin/zsh


PE_X=$1
PE_Y=$2
BENCH=$3
RATE=$4
SEED=$5
BURST=$6

pushd bin/rtflow_random_analysis/test_${1}_${2}_${3}_${4}_${5}_${6}

cp ../../../add.py .
python add.py

pwd
popd

