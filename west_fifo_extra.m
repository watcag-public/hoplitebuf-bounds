## Copyright (C) 2018 rpellizz
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <https://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {} {@var{retval} =} west_fifo (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: rpellizz <rpellizz@DESKTOP-1A3KAMF>
## Created: 2018-09-24

function [retval B FlightLatencyW TotalLatencyS] = west_fifo_extra (burstW, rhoW, burstS, rhoS, burstE, rhoE, firstPacketOnly)
  %WEST_FIFO_EXTRA Computes latencies for HopliteBuf vertical ring W -> S design
  %
  %burstW: N x N array of burstiness of flows that arrive from W and turn S
  %rhoW: N x N array of rates of flows that arrive from W and turn S
  %burstS: N x N array of burst of flows that go from PE to S
  %rhoS: N x N array of rates of flows that go from PE to S
  %burstE: N x 1 vector of cumulative burstiness of flows that go from PE to E
  %rhoE: N x 1  vector of cumulative rates of flows that go from PE to E
  %for all inputs, row is entry, and column is exit
  %firstPacketOnly: 1 to compute injection latency for first packet only;
  %                 0 to compute injection latency for all packets in the burst
  %
  %returns
  %retval: true if we have a bound; false if we have no bound
  %B: max buffer size for each router
  %FlightLatencyW: max in-flight latency (buffer to exit) for flows from W to S
  %TotalLatencyS: total latency (injection + in-flight) for flows from PE to S
  %
  %Note: burst = 0 means that the flow does not exist. 
  %burst must otherwise be >=1
  %rate for a flow with burst = 0 MUST be 0
  %otherwise the rate must be between 0 and 1 (extremes excluded)
  
  retval = false; FlightLatencyW = 0; TotalLatencyS = 0; B = 0;
  
   if(nargin < 4 | nargin == 5)
    'not enough input arguments'
    return
  endif
  
  [N N1] = size(burstW);
  
  if(nargin == 4)
    burstE = zeros(N, 1);
    rhoE = zeros(N, 1);
  endif
  
  if(nargin < 7)
    firstPacketOnly = 1;
  endif
  
  [N2 N3] = size(rhoW);
  [N4 N5] = size(burstS);
  [N6 N7] = size(rhoS);
  [N8 N9] = size(burstE);
  [N10 N11] = size(rhoE);
  if (N != N1 | N != N2 | N != N3 | N != N4 | N != N5 | N != N6 | N != N7 | N != N8 | N != N10 | N9 != 1 | N11 != 1 )
    'sizes do not match'
    return
  endif
  
  %note: no check is performed to ensure that burstiness and rate values 
  %are valid to avoid slowing down the analysis
  
  sigmaW = burstW - rhoW;
  sigmaS = burstS - rhoS;
  
  R = ones(N,1);
  %this term captures R(i), remaining bandwidth at node i
  for i = 1:N
	  for p = 1:i - 1
		  for l = i:N
			  R(i) = R(i) - rhoW(p,l) - rhoS(p,l);	
      endfor
		  for l = 1:p - 1
			  R(i) = R(i) - rhoW(p,l) - rhoS(p,l);
      endfor
    endfor
	  for p = i + 1:N
		  for l = i:p - 1
			  R(i) = R(i) - rhoW(p,l) - rhoS(p,l);
      endfor
    endfor
  endfor

  Rij = zeros (N, N);
  %this term captures R(i,j), remaining bandwidth for flow i->j
  for i = 1:N
	  for j = 1:N
		  Rij(i,j) = R(i) - sum( rhoW(i, :) ) + rhoW(i,j);
    endfor
  endfor
  
  
  sigma_l = zeros(N*N, 1);
  %Linearize the sigmaW into a vector of size N*N
  for i = 1:N
	  for j = 1:N
		  sigma_l(i*N + j - N) = sigmaW(i,j);
    endfor
  endfor
  
  Apartial = zeros(N,N*N);
  Extra = zeros(N, 1);
  %these two terms captures T(i), the service delay at node i. 
  %Extra(i) is the costant term depending on S flows
  %Apartial is a multiplication matrix to solve the system of sigma'
  for i = 1:N
	  for p = 1:i - 1
		  for l = i:N
			  Apartial(i,p*N + l - N) = 1 / R(i);
        Extra(i) = Extra(i) + sigmaS(p,l) / R(i); 
      endfor
		  for l = 1:p - 1
			  Apartial(i,p*N + l - N) = 1 / R(i);
        Extra(i) = Extra(i) + sigmaS(p,l) / R(i); 
      endfor
    endfor
	  for p = i + 1:N
		  for l = i:p - 1
			  Apartial(i,p*N + l - N) = 1 / R(i);
        Extra(i) = Extra(i) + sigmaS(p,l) / R(i); 
      endfor
    endfor
  endfor
  
  rho_AT = zeros(N*N, N*N);
  %full multiplication matrix to solve the sigma' system; 
  %note T(i) must be multiplied by rhoW(i,j) for each flow under analysis
  for i = 1:N
	  for j = 1:N
		  rho_AT(i*N + j - N, :) = Apartial(i, :) * rhoW(i,j);
    endfor
  endfor
  
  CTpartial = zeros(N, 1);
  %contains the constant terms sum_{l} sigma(i,l) / R(i) in T(i,j)
  %in other words, this are W flows entering at i
  %note we also sum the one under analysis; this will be taken out in CT
  for i = 1:N
	  for l = 1:N
		  CTpartial(i) = CTpartial(i)+ sigmaW(i,l) / R(i);
    endfor
  endfor

  CT = zeros(N*N, 1);
  rho_CT = zeros (N*N,1);
  %all constant terms that end in T(i,j)
  %we use Extra(i) + CTpartial(i), where we have to take out the f.u.a.
  %rho_CT is then scaled by rho(i,j)
  for i = 1:N
	  for j = 1:N
		  CT(i*N + j - N) = CTpartial(i) - sigmaW(i,j) / R(i) + Extra(i);
		  rho_CT(i*N + j - N) = rhoW(i,j) * CT(i*N + j - N);
    endfor
  endfor


  %can we invert? NOTE: BEWARE OF NUMERICAL ERRORS
  if (det(eye(N*N) - rho_AT) == 0)
    'singular matrix - unstable network'
    return
  endif

  %compute result for S flows in linearized array
  sigma_l_result = (eye(N*N) - rho_AT)\(sigma_l + rho_CT);

  %can't have negative burstiness, this indicates infeasibility...
  if (any(sigma_l_result < 0))
    'negative sigma - unstable network'
    return
  endif
  
  %determine Latency for west flows
  %delay is sigma(i,j) / R(i,j) + T(i,j) 
  %for latency add hop distance
  FlightLatencyW = zeros(N, N);
  for i = 1:N
	  for j = 1:N
      if(burstW(i,j) == 0)
        FlightLatencyW(i,j) = 0;
      elseif( rhoW(i,j) > Rij(i,j) )
        'exceed link utilization - unstable network'
        return
      else
        if(j >= i)
          Delta = j - i + 1;
        else
          Delta = j - i + 1 + N;
        endif
  
		    Tij = Apartial(i,:)*sigma_l_result + CT(i*N + j - N);
		    FlightLatencyW(i,j) = sigmaW(i,j) / Rij(i,j) + Tij + Delta;
      endif
    endfor
  endfor
  %floor the latencies 
  FlightLatencyW = floor(FlightLatencyW);
		
  %compute buffer size
  %buffer size is sum of sigma of W flows entering + sum of rho * T(i)
  B = zeros(N, 1);
  for i = 1:N
    if( sum( burstW(i, :) ) == 0)
      B(i) = 0;
    elseif( sum ( rhoW(i, :) ) > R(i) )
      'exceed link utilization - unstable network'
      return
    else
	    B(i) = sum( sigmaW(i, :) ) + ( Apartial(i,:)*sigma_l_result + Extra(i) ) * sum ( rhoW(i, :) );
    endif
  endfor
  %floor buffer sizes, since it has to be integer
  B = floor(B);
  
  %compute the interfering rate from N->S and W->S flows on S port of node i
  interrho = zeros(N, 1);
  for i = 1:N
      interrho(i) = R(i) - sum ( rhoW(i,:) );
  endfor
  
  %compute the interfering sigma from N->S and W->S on S port of each node i
  intersigma = zeros(N, 1);
  for i = 1:N
    %N->S
	  for p = 1:i - 1
		  for l = i:N
			  intersigma(i) = intersigma(i) + sigma_l_result(p*N + l - N) + sigmaS(p,l);	
      endfor
		  for l = 1:p - 1
			  intersigma(i) = intersigma(i) + sigma_l_result(p*N + l - N) + sigmaS(p,l);
      endfor
    endfor
	  for p = i + 1:N
		  for l = i:p - 1
			  intersigma(i) = intersigma(i) + sigma_l_result(p*N + l - N) + sigmaS(p,l);
      endfor
    endfor
    %W->S
    intersigma(i) = intersigma(i) + sum( sigma_l_result(i*N + 1 - N : i*N) ); 
  endfor
  
  %apply Theorem 2
  TotalLatencyS = zeros(N, N);
  for i = 1:N
    for j = 1:N
      
      if(rhoS(i,j) == 0)
        TotalLatencyS(i,j) = 0;
      else
        if(j >= i)
          Delta = j - i + 1;
        else
          Delta = j - i + 1 + N;
        endif
      
        %conflicting b. Add to intersigma the PE->E, and all other PE->S
        confb = ceil(intersigma(i) + 1 + interrho(i)) + burstE(i) + sum( burstS(i, :) ) - burstS(i,j);
        %conflicting rho. Add to interrho the PE->E, and all other PE->S
        confrho = interrho(i) - rhoE(i) - sum( rhoS(i, :) ) + rhoS(i,j);
        
        if(confrho <= 0)
          'saturated S link - unstable network'
          return
        endif
        
        TotalLatencyS(i,j) = compute_injection_latency (burstS(i,j), rhoS(i,j), confb, confrho, firstPacketOnly) + Delta;
      endif
      
    endfor
  endfor
  
  
  retval = true;

endfunction
