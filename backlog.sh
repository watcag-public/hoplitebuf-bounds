#!/bin/zsh


echo "r, tb, m, stable, maxfifo, wclatency" > backlog.csv
for r in 0.01 0.05 0.1 0.15 0.2 0.24 0.24999999 0.25 0.3 0.32 0.33
do
  for tb in 0 1
  do
    for m in 0
    do
      ./analyze_bp.py -N 3 -f fig11.dat -R $r -M 0 -TB $tb > /tmp/data.$r.$tb.$m | cat
      stable=`cat /tmp/data.$r.$tb.$m | grep ^Stable | cut -d"," -f 1 | cut -d"=" -f2`
      maxfifo=`cat /tmp/data.$r.$tb.$m | grep ^Stable | cut -d"," -f 2 | cut -d"=" -f2`
      wclatency=`cat /tmp/data.$r.$tb.$m | grep ^Stable | cut -d"," -f 3 | cut -d"=" -f2`
      echo $r, $tb, $m, $stable, $maxfifo, $wclatency >> backlog.csv
    done
  done
done
