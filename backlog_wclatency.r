library(ggplot2)
library(scales)


backlog <- read.csv("backlog.csv", header=TRUE, sep=",")
backlog<-backlog[backlog$m==0,]
backlog<-backlog[backlog$stable==1,]

backlog$method<-""

backlog[backlog$tb==1,]$method<-"Bklg"
backlog[backlog$tb==0,]$method<-"TS"

pdf(file="backlog_wclatency.pdf", height=2.5, width=3)

p <- ggplot(backlog, aes(x=r,y=wclatency,group=factor(method),color=factor(method)))
p <- p + geom_line(size=1.5) 
p <- p + scale_y_log10()
p <- p + xlab("Injection Rate")
p <- p + ylab("Worst-Case Latency")

p + theme_bw() %+replace% theme(axis.title=element_text(),axis.title.y=theme_bw()$axis.title.y) +
	theme(
	  axis.line=element_line(color='black'),
	  legend.position=c(0.5,0.8),
	  #legend.position="top",
	  legend.background=element_rect(fill="transparent"),
	  axis.title.x = element_text(size=10),
	  axis.title.y = element_text(size=10),
	  panel.border=element_blank(),
	  panel.grid.minor = element_blank(),
	  panel.grid.major.x = element_blank(),
	  legend.key.width=unit(0.5,"cm"),
	  legend.key.height=unit(0.5,"cm"),
	  plot.title = element_text(hjust = 0.5),
	  legend.key = element_blank(),
	  legend.title=element_text(size=10),
	  axis.text.y=element_text(size=10,color='black'),
	  axis.text.x=element_text(angle=0,hjust=0.5, size=10, color='black'),
	  legend.text=element_text(size=10))+
guides(
       color=guide_legend(ncol=1,title="")
       );

