from pymake2 import *
Debug = True # <-- set it to True to enable more debugging messages 
HighlightErrors = True # To enable the highliter
HighlightWarnings = True # To enable the highliter
HighlightNotes = True # To enable the highliter
latexfile = 'HopliteRT.tex'
pdffile = 'HopliteRT.pdf'

@target
def all(pdf):
    printcolor('Build Succeded')
    return True

@target
def pdf(latexfile):
    if run(eval('pdflatex -shell-escape -halt-on-error $(latexfile)'), True, True):
        printcolor('Build Succeded', fg='32', B=True)
        run(eval('evince $(pdffile)&'))
        return True

@target
def clean():
    retV = run(eval('rm -f *.aux *.log *.blg *.bbl *.synctex.gz *.out $(pdffile) *.vtc'), True)
    return retV

