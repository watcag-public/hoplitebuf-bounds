#!/usr/bin/env python

import commands as CMD
import numpy as np
import sys
import math
import argparse
import csv
import pandas as pd

parser = argparse.ArgumentParser(description='HopliteRT Analyzer')
parser.add_argument('-f', metavar='flowset file', type=str, default="flow.dat", help='a file containing a list of flows')


args = parser.parse_args()
file_name = args.f
df = pd.read_csv(file_name, sep=',\s+', engine='python')
length = len(df.index)
debuglog = open("log.csv","w")

inflight = df['Rlat'].max()
tot_ws = df['TootalWS'].max()
tot_we = df['TootalWE'].max()
srcq = tot_ws + tot_we
stable = df['stable'].min()

debuglog.write("%d,%d,%d" % (inflight, srcq, stable))

debuglog.close()
