#!/usr/bin/env python

import commands as CMD
import numpy as np
import sys
import math
import argparse
import csv
import pandas as pd

parser = argparse.ArgumentParser(description='HopliteRT Analyzer')
parser.add_argument('-f', metavar='flowset file', type=str, default="flow.dat", help='a file containing a list of flows')
parser.add_argument('-B', metavar='burst info', type=int, default=0, help='burst, by default reads from input file')
parser.add_argument('-R', metavar='rate info', type=float, default=0.00, help='rate, by default reads from input file')
args = parser.parse_args()


file_name = args.f
burst_user = args.B
rate_user = args.R

df = pd.read_csv(file_name, sep=',\s+', engine='python')

length = len(df.index)

for i in range(length):
	df['B'][i] = burst_user
	df['R'][i] = rate_user

 
del df['=>']
del df['Rlat']
del df['Rlat-opt']
del df['WS']
del df['WE']
del df['stable']
del df['TootalWS']
del df['TootalWE']
del df['period']
del df['sustained-WS']
del df['sustained-WE']

df.to_csv ('test.dat', sep=',',index = None, header=False) 
#print(df)
