## Copyright (C) 2018 rpellizz
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <https://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {} {@var{retval} =} compute_injection_latency (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: rpellizz <rpellizz@DESKTOP-1A3KAMF>
## Created: 2018-09-25

function retval = compute_injection_latency (burst, rho, confb, confrho)

        %compute the injection latency of a flow. Here:
        %burst, rho are the burstiness and rate of the flow under analysis
        %confb, confrho are the burstiness and rate of all conflicting flows
        
        %for a flow injected to the E port, the conflicting flows are:
        %1. any other flow injected by the same PE (to the E or to the S)
        %2. any other flow crossing the switch W->E
        
        %for a flow injected to the S port, the conflicting flows are:
        %1. any other flow injected by the same PE (to the E or to the S)
        %2. any other flow coming from N->S
        %4. any other flow turning W->S

        firstpacket = ceil(confb/confrho) - 1 + ceil(1/rho);
        %furtherpackets = ceil( (burst - 1) * max( 1/rho, 1/confrho ) );
        furtherpackets = 0;
	retval = firstpacket + furtherpackets;

endfunction
